package ffmpeg01.itsen.com.ffmpeg01;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.itsen.base.BaseAdapter;
import com.itsen.base.BaseAppActivity;
import com.itsen.base.LDividerItemDecoration;
import com.itsen.model.mp32pcm.PcmActivity;
import com.itsen.model.mp42yuv.YuvActivity;
import com.itsen.model.opengl.OpenGLActivity;
import com.itsen.model.opengl2.OpenGlES2Activity;
import com.itsen.model.opensl.OpenSlESActivity;
import com.itsen.model.player.PlayerActivity;
import com.itsen.model.player2.Player2Activity;
import com.itsen.model.vaplayer.AvPlayerActivity;

public class MainActivity extends BaseAppActivity {

    private RecyclerView recyclerView;
    private BaseAdapter adapter;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initData() {
        intiIntentList();
        recyclerView = (RecyclerView) findViewById(R.id.rv_main);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new LDividerItemDecoration(this, LDividerItemDecoration.VERTICAL_LIST));
        adapter = new BaseAdapter(listItenText);
        adapter.setOnitemClickListener(new BaseAdapter.OnitemClickListener() {
            @Override
            public void itemClick(View v, int position) {
                startActivity(intentList.get(position).putExtra(KEY, listItenText.get(position)));
            }
        });
        recyclerView.setAdapter(adapter);
        setToolbarTitle("ffmpeg案例列表");
        setToolbarNavGone();
    }

    @Override
    public void initListData(){
        addIntent("视频解码YUV",new Intent(this, YuvActivity.class));
        addIntent("视频播放",new Intent(this, PlayerActivity.class));
        addIntent("音乐解码pcm",new Intent(this, PcmActivity.class));
        addIntent("音乐播放",new Intent(this, Player2Activity.class));
        addIntent("OpenSL ES",new Intent(this, OpenSlESActivity.class));
        addIntent("音频同步",new Intent(this, AvPlayerActivity.class));
        addIntent("OpenGL ES 1.0",new Intent(this, OpenGLActivity.class));
        addIntent("OpenGL ES 2.0",new Intent(this, OpenGlES2Activity.class));
    }
}
