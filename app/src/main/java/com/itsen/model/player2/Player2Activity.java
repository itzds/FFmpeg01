package com.itsen.model.player2;

import android.os.Environment;
import android.view.View;

import com.itsen.base.BaseAppActivity;

import java.io.File;

import ffmpeg01.itsen.com.ffmpeg01.R;

public class Player2Activity extends BaseAppActivity {

    @Override
    public int getLayoutId() {
        return R.layout.activity_player2;
    }

    @Override
    public void initListData() {

    }

    @Override
    public void initData() {

    }

    public void click(View view) {
       final String input = new File(Environment.getExternalStorageDirectory(),"input.mp3").getAbsolutePath();
       final String output = new File(Environment.getExternalStorageDirectory(),"output.pcm").getAbsolutePath();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Player player = new Player();
                player.sound(input,output);
            }
        }).start();
    }
}
