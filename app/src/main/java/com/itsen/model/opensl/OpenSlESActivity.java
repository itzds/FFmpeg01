package com.itsen.model.opensl;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.itsen.base.BaseAppActivity;

import ffmpeg01.itsen.com.ffmpeg01.R;

public class OpenSlESActivity extends BaseAppActivity {

    private Player player;

    @Override
    public int getLayoutId() {
        return R.layout.activity_open_sl_es;
    }

    @Override
    public void initListData() {

    }

    @Override
    public void initData() {

    }


    public void click(View view) {
        player = new Player();
        player.openslplayer();
    }

    public void stop(View view) {
        player.openslpStop();
    }
}
