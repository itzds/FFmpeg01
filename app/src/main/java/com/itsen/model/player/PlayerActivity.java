package com.itsen.model.player;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.itsen.base.BaseAppActivity;

import java.io.File;

import ffmpeg01.itsen.com.ffmpeg01.R;

public class PlayerActivity extends BaseAppActivity {
    Spinner spinner;
    VideoView videoView;
    @Override
    public int getLayoutId() {
        return R.layout.activity_player;
    }

    @Override
    public void initListData() {

    }

    @Override
    public void initData() {
        spinner = findViewById(R.id.sp_select);
        videoView = findViewById(R.id.video_view);
        String[] videoArray = getResources().getStringArray(R.array.video_list);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1, videoArray);
        spinner.setAdapter(adapter);
    }

    public void play(View view) {
        String video = spinner.getSelectedItem().toString();
        String input = new File(Environment.getExternalStorageDirectory(), video).getAbsolutePath();
        videoView.player(input);
    }
}
