package com.itsen.model.opengl;

import android.opengl.GLSurfaceView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import ffmpeg01.itsen.com.ffmpeg01.R;

public class OpenGLActivity extends AppCompatActivity {

    private AbstractMyRenderer render;
    private GLSurfaceView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_gl);
        view = findViewById(R.id.gl_view);
        //render = new MyTriangleRenderer();
        //render = new MyPointRenderer1();
        //render = new MyPointSizeRenderer();
        //render = new MyLineStripRenderer();
        render = new MyTriangleConeRenderer();
        view.setRenderer(render);
        //GLSurfaceView.RENDERMODE_CONTINUOUSLY:持续渲染(默认)
        //GLSurfaceView.RENDERMODE_WHEN_DIRTY:脏渲染,命令渲染
        view.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    public void click(View view) {
        float step = 5f;
        switch (view.getId()) {
            case R.id.btn_right:
                render.yrotate = render.yrotate - step;
                break;
            case R.id.btn_left:
                render.yrotate = render.yrotate + step;
                break;
            case R.id.btn_up:
                render.xrotate = render.xrotate - step;
                break;
            case R.id.btn_down:
                render.xrotate = render.xrotate + step;
                break;
            default:
                break;
        }
        //请求渲染,和脏渲染配合使用
        this.view.requestRender();
    }
}
