package com.itsen.model.mp42yuv;

import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.itsen.base.BaseAppActivity;
import com.unistrong.yang.zb_permission.Permission;
import com.unistrong.yang.zb_permission.ZbPermission;
import com.unistrong.yang.zb_permission.ZbPermissionFail;
import com.unistrong.yang.zb_permission.ZbPermissionSuccess;

import java.io.File;

import ffmpeg01.itsen.com.ffmpeg01.R;

public class YuvActivity extends BaseAppActivity {
    private final int REQUEST_CONTACT = 50;
    private final int REQUEST_STORAGE = 100;
    private final int REQUEST_CAMERA = 200;
    static {
        System.loadLibrary("avcodec-56");
        System.loadLibrary("avfilter-5");
        System.loadLibrary("avformat-56");
        System.loadLibrary("avutil-54");
        System.loadLibrary("swresample-1");
        System.loadLibrary("swscale-3");
        System.loadLibrary("native-lib");
    }

    private TextView tv_show;
     @Override
    public int getLayoutId() {
        return R.layout.activity_yuv;
    }

    @Override
    public void initListData() {
        tv_show = (TextView) findViewById(R.id.tv_show);
    }

    @Override
    public void initData() {

    }

    public native void open(String inputStr, String outStr);
    public native void result(String result);
    StringBuffer buffer = new StringBuffer();

    public  void outputRest(String result){
       buffer.append(result+"/n");
       tv_show.setText(buffer);
    }

    public void load(View view) {
        ZbPermission.needPermission(YuvActivity.this, REQUEST_STORAGE, Permission.STORAGE);
    }

    @ZbPermissionSuccess(requestCode = REQUEST_STORAGE)
    public void permissionSuccess() {
        Toast.makeText(YuvActivity.this, "成功授予读写权限注解" , Toast.LENGTH_SHORT).show();

        File file = new File(Environment.getExternalStorageDirectory(), "input.mp4");
        String input = file.getAbsolutePath();
        Log.e("path",input);
        if (file.exists()){
            Toast.makeText(this,input,Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(this,"文件不存在",Toast.LENGTH_LONG).show();
        }
        String  output= new File(Environment.getExternalStorageDirectory(), "output.yuv").getAbsolutePath();
        //调用native方法
        open(input, output);
    }

    @ZbPermissionFail(requestCode = REQUEST_STORAGE)
    public void permissionFail() {
        Toast.makeText(YuvActivity.this, "授予读写权限失败注解" , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ZbPermission.onRequestPermissionsResult(YuvActivity.this,requestCode,permissions,grantResults);
    }
}
