package com.itsen.model.vaplayer;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;

import com.itsen.base.BaseAppActivity;

import java.io.File;

import ffmpeg01.itsen.com.ffmpeg01.R;

public class AvPlayerActivity extends BaseAppActivity {
    ItPlayer player;
    SurfaceView surfaceView;
    @Override
    public int getLayoutId() {
        return R.layout.activity_av_player;
    }

    @Override
    public void initListData() {

    }

    @Override
    public void initData() {
        surfaceView = (SurfaceView) findViewById(R.id.surface);
        player = new ItPlayer();
        player.setSurfaceView(surfaceView);
    }

    public void player(View view) {
        File file = new File(Environment.getExternalStorageDirectory(), "input.mp4");
       // player.playJava("rtmp://live.hkstv.hk.lxdns.com/live/hks");
        //player.playJava("rtmp://192.168.150.130/myapp/mystream");
        player.playJava("rtmp://192.168.0.7:1235/myapp/mystream");
        //player.playJava(file.getAbsolutePath());
    }

    public void stop(View view)  {
        player.release();
    }
}
