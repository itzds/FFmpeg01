package com.itsen.model.opengl2.render;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by zds
 * Created time 2018/9/4 14:46
 * Description:FFmpeg01
 * Version: V 1.0
 */

public abstract class BaseRender implements GLSurfaceView.Renderer {
    public float ratio;
    public int mProgram;
    /**
     * 表示三维坐标点
     */
    static final int COORDS_PER_VERTEX = 3;

    /**
     * @param type       着色器类型
     * @param shaderCode 着色器代码
     * @return
     */
    public int createShader(int type, String shaderCode) {
        //根据type创建顶点着色器或者片元着色器
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
        return shader;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        //设置清屏色
        GLES20.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        ratio = (float)width/height;
        //设置视口大小
        GLES20.glViewport(0, 0, width, height);
    }

    @Override
    public abstract void onDrawFrame(GL10 gl);
}
