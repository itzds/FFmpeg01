package com.itsen.model.opengl2.render;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by zds
 * Created time 2018/9/4 16:42
 * Description:三角形
 * Version: V 1.0
 */

public class ShapeTriangle extends BaseRender {
    /**
     * 缓冲区
     */
    private FloatBuffer vertexBuffer;

    /**
     * 顶点着色器
     */
    private final String vertexShaderCode =
            "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = vPosition;" +
                    "}";

    /**
     * 片源着色器
     */
    private final String fragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";

    private int mProgram;


    /**
     * 绘制的点数组 top \ bottom left \ bottom right
     */
    static float triangleCoords[] = {
            0.5f, 0.5f, 0.0f,
            -0.5f, -0.5f, 0.0f,
            0.5f, -0.5f, 0.0f
    };

    /**
     * 顶点着色器的vPosition成员句柄
     */
    private int mPositionHandle;

    /**
     * 片元着色器的vColor成员的句柄
     */
    private int mColorHandle;

    /**
     * 顶点个数
     */
    private final int vertexCount = triangleCoords.length / COORDS_PER_VERTEX;
    /**
     * 顶点之间的偏移量 // 每个顶点四个字节
     */
    private final int vertexStride = COORDS_PER_VERTEX * 4;
    /**
     *设置颜色，依次为红绿蓝和透明通道
     */
    float color[] = {1.0f, 1.0f, 1.0f, 1.0f};


    //1、创建

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl,config);
        //设置清屏色
        //GLES20.glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
        int vertexShader = createShader(GLES20.GL_VERTEX_SHADER,
                vertexShaderCode);
        int fragmentShader = createShader(GLES20.GL_FRAGMENT_SHADER,
                fragmentShaderCode);

        //创建一个空的OpenGLES程序
        mProgram = GLES20.glCreateProgram();
        //将顶点着色器加入到程序
        GLES20.glAttachShader(mProgram, vertexShader);
        //将片元着色器加入到程序中
        GLES20.glAttachShader(mProgram, fragmentShader);
        //连接到着色器程序
        GLES20.glLinkProgram(mProgram);
    }

    //改变
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl,width,height);
    }

    //绘制
    @Override
    public void onDrawFrame(GL10 gl) {

        float triangleCoords[] = {
                0f, ratio, 0.0f,
                -1f, -ratio, 0.0f,
                1f, -ratio, 0.0f
        };

        //分配字节缓存区空间,存放顶点坐标数据
        ByteBuffer bb = ByteBuffer.allocateDirect(
                triangleCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());

        //将坐标数据转换为FloatBuffer，用以传入给OpenGL ES程序
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(triangleCoords);
        vertexBuffer.position(0);



        //清除颜色缓冲区和深度缓冲区
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT|GLES20.GL_DEPTH_BUFFER_BIT);

        //将程序加入到OpenGLES2.0环境
        GLES20.glUseProgram(mProgram);

        //获取顶点着色器的vPosition成员句柄
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");

        //启用三角形顶点的句柄
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        //准备三角形的坐标数据
        GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        //获取片元着色器的vColor成员的句柄
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");

        //设置绘制三角形的颜色
        GLES20.glUniform4fv(mColorHandle, 1, color, 0);

        //绘制三角形
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

        //禁止顶点数组的句柄
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }

}
