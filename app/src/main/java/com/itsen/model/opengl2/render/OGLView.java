package com.itsen.model.opengl2.render;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

/**
 * Created by zds
 * Created time 2018/9/4 15:29
 * Description:FFmpeg01
 * Version: V 1.0
 */

public class OGLView extends GLSurfaceView {

    public OGLView(Context context) {
        this(context,null);
    }

    public OGLView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        //TODO 设置版本
        setEGLContextClientVersion(2);
    }

    public void setMyRenderer(BaseRender render) {
        setRenderer(render);
        //TODO 渲染模式
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }
}
