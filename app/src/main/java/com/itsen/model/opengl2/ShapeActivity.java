package com.itsen.model.opengl2;

import android.opengl.GLSurfaceView;

import com.itsen.base.BaseAppActivity;
import com.itsen.model.opengl2.render.OGLView;
import com.itsen.model.opengl2.render.ShapeCube;
import com.itsen.model.opengl2.render.ShapeOval;
import com.itsen.model.opengl2.render.ShapeSquare;
import com.itsen.model.opengl2.render.ShapeTriangle;
import com.itsen.model.opengl2.render.ShapeTriangle2;

import ffmpeg01.itsen.com.ffmpeg01.R;

/**
 * Created by zds
 * Created time 2018/9/4 15:18
 * Description:正三角形
 * Version: V 1.0
 */

public class ShapeActivity extends BaseAppActivity {
    private OGLView oglView;
    private GLSurfaceView glSurfaceView;
    @Override
    public int getLayoutId() {
        return R.layout.activity_shape;
    }

    @Override
    public void initListData() {

    }

    @Override
    public void initData() {
        oglView = findViewById(R.id.opengl_view);
        //oglView.setMyRenderer(new ShapeTriangle());
        //oglView.setMyRenderer(new ShapeTriangle2());
        //oglView.setMyRenderer(new ShapeSquare());
        //oglView.setMyRenderer(new ShapeOval());
        oglView.setMyRenderer(new ShapeCube());

        /*glSurfaceView = findViewById(R.id.opengl_view);
        glSurfaceView.setEGLContextClientVersion(2);
        glSurfaceView.setRenderer(new ShapeTriangle());
        glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        oglView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        oglView.onPause();
    }
}
