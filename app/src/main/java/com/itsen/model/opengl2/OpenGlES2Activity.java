package com.itsen.model.opengl2;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.itsen.base.BaseAdapter;
import com.itsen.base.BaseAppActivity;
import com.itsen.base.LDividerItemDecoration;
import com.itsen.model.opengl.OpenGLActivity;

import ffmpeg01.itsen.com.ffmpeg01.R;

public class OpenGlES2Activity extends BaseAppActivity {
    private RecyclerView recyclerView;
    private BaseAdapter adapter;
    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initListData() {
        addIntent("OpenGL ES 1.0",new Intent(this, ShapeActivity.class));
    }

    @Override
    public void initData() {
        intiIntentList();
        recyclerView = findViewById(R.id.rv_main);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new LDividerItemDecoration(this, LDividerItemDecoration.VERTICAL_LIST));
        adapter = new BaseAdapter(listItenText);
        adapter.setOnitemClickListener(new BaseAdapter.OnitemClickListener() {
            @Override
            public void itemClick(View v, int position) {
                startActivity(intentList.get(position).putExtra(KEY, listItenText.get(position)));
            }
        });
        recyclerView.setAdapter(adapter);
        setToolbarTitle("OpenGL ES 2.0");
    }

}
