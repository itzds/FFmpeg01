package com.itsen.base;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ffmpeg01.itsen.com.ffmpeg01.R;

public class BaseAdapter extends RecyclerView.Adapter<BaseAdapter.MyHolder> {

    List<String> list;
    private OnitemClickListener onitemClickListener;

    public BaseAdapter(List<String> list) {
        this.list = list;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main,parent,false);
        MyHolder myHolder = new MyHolder(view);
        return myHolder;
    }
    //数据绑定
    @Override
    public void onBindViewHolder(MyHolder myHolder, int position) {
          myHolder.textView.setText(list.get(position));
    }

    //条数
    @Override
    public int getItemCount() {
        return list.size();
    }

    //继承RecycleView的holder
    class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView textView;
        private LinearLayout item;
        public MyHolder(View itemView) {
            super(itemView);
            item = (LinearLayout) itemView.findViewById(R.id.item_main);
            textView = (TextView) itemView.findViewById(R.id.tv_intent);
            item.setOnClickListener(this); //给每个条目添加点击事件
        }

        @Override
        public void onClick(View v) {
             if(onitemClickListener!=null){
                onitemClickListener.itemClick(v,getAdapterPosition());
             }
        }
    }

    /**
     * 定义接口
     */
    public interface OnitemClickListener{
        void itemClick(View v, int position);
    }

    /**
     * 设置接口
     * @param onitemClickListener
     */
    public void setOnitemClickListener(OnitemClickListener onitemClickListener) {
        this.onitemClickListener = onitemClickListener;
    }
}
