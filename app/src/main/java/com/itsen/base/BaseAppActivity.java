package com.itsen.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextPaint;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ffmpeg01.itsen.com.ffmpeg01.R;


/**
 * Created by zhoudesen
 * Created time 2017/5/18 11:09
 * Description:基类
 */

public abstract class BaseAppActivity extends AppCompatActivity {
    public List<Intent> intentList;
    public List<String> listItenText;
    public abstract int getLayoutId();
    public abstract void initListData();
    private final static String TAG = "BaseAppActivity";
    public abstract void initData();
    private Toolbar toolbar;
    private Intent intent;
    public final static String KEY = "baseIntent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(getLayoutId());
        intent = getIntent();
        initToolbar();
        setToolbarTitle(getExtraString(KEY));
        initData();
    }

    /**
     * toolbar初始化
     */
    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_c_ly);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    /**
     * 设置标题
     *
     * @param title
     */
    public void setToolbarTitle(String title) {
        if (toolbar != null) {
            TextView textView = (TextView) toolbar.findViewById(R.id.tb_title);
            if (textView!=null){
                TextPaint tp = textView.getPaint();
                tp.setFakeBoldText(true);
                textView.setText(title);
            }
        }
    }
    /**
     * 显示左边
     *
     */
    public void showRightButton(String text){
        Button button = (Button) toolbar.findViewById(R.id.btn_right);
        if (button !=null){
            button.setVisibility(View.VISIBLE);
            button.setText(text);
        }
    }

    /**
     * 隐藏返回按钮
     */
    public void setToolbarNavGone() {
        if (toolbar != null) {
            toolbar.setNavigationIcon(null);
        }
    }

    /**
     *
     * @return  toolbar
     */
    public Toolbar getToolbar(){
        return toolbar;
    }


    /**
     * 获取value
     *
     * @param key
     * @return
     */
    public String getExtraString(String key) {
        if (intent != null) {
            if(intent.getStringExtra(key)==null){
                new IllegalAccessError("无法获取key="+key+"的对应值");
            }
            return intent.getStringExtra(key);
        }
        return "";
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG,"onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG,"onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG,"onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG,"onRestart");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG,"onStart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG,"onDestroy");
    }

    public void btnClisk(View view) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void intiIntentList() {
        intentList = new ArrayList<>();
        listItenText = new ArrayList();
        initListData();
    }


    public void addIntent(String tip,Intent intent){
        if (intentList==null){
            //需要用到列表的activity调用
            new Throwable("请先初始化intiIntentList方法！").printStackTrace();
        }
        if (intent!=null){
            intentList.add(intent);
            listItenText.add(tip);
        }
    }
}
